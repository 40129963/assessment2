﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace cw2
{
    /// <summary>
    /// Written by 40129963
    /// Nov-Dec 2014
    /// 
    /// university acts as a facade between the GUI and the student, staff and module classes.
    /// </summary>
    public class university
    {
        // Private singleton constructor, to prevent multiple instances of university
        private university() { }

        //static instance of university, so a new instance is never created
        private static university instance = new university();
        public static university getInstance() { return instance; }

        private student stu;
        private staff stf;
        private module mod;

        /// <summary>
        /// This method calls the createUpdate method from either Student or Staff class,
        /// depending on the student_staff parameter. 
        /// </summary>
        /// <param name="student_staff">0 indicates a Student, 1 indicates a Staff</param>
        /// <param name="matric_payroll">Either the Matric or Payroll number</param>
        /// <param name="fname">First Name</param>
        /// <param name="lname">Last Name</param>
        /// <param name="address">Full Address</param>
        /// <param name="email">Email Address</param>
        /// <param name="dept">Optional parameter: Department</param>
        /// <param name="role">Optional parameter: Role</param>
        /// <returns>Boolean Array indicating which parameters were succesfully INSERTed or UPDATEd</returns>
        public bool[] createUpdateStudent_Staff(int student_staff, int matric_payroll, string fname, string lname, string address, string email, string dept = null, string role = null)
        {
            bool[] validation = null;
            switch (student_staff)
            {
                case 0:
                    stu = new student();
                    validation = stu.createUpdate(matric_payroll, fname, lname, address, email);
                    break;
                case 1:
                    stf = new staff();
                    validation = stf.createUpdate(matric_payroll, fname, lname, address, email, dept, role);
                    break;
            }
            return validation;
        }

        /// <summary>
        /// This method calls the createUpdate method from module class
        /// </summary>
        /// <param name="code">module code</param>
        /// <param name="name">module name</param>
        /// <param name="leader">payroll number for module leader</param>
        /// <returns>Boolean Array indicating which parameters were succesfully INSERTed or UPDATEd</returns>
        public bool[] createUpdateModule(string code, string name, int leader)
        {
            List<List<string>> students_staffs = existingStudent_Staff(1);
            int testPayroll = -1;
            if (leader >= 0 &&
                leader <= students_staffs.Count)
            {
                List<string> leaderDetails = students_staffs[leader];
                testPayroll = int.Parse(leaderDetails[0]);
            }
            mod = new module();
            return mod.createUpdate(code, name, testPayroll);
        }

        /// <summary>
        /// This method calls the enroleUpdate method from module class
        /// </summary>
        /// <param name="moduleCode">module code</param>
        /// <param name="student">index for enrolled student</param>
        /// <param name="mark">Optional parameter: mark for enrolled student</param>
        /// <returns>Boolean Array indicating which parameters were succesfully INSERTed or UPDATEd</returns>
        public bool[] enroleUpdate(string moduleCode, int student, int mark = -1)
        {
            List<List<string>> students_staffs = existingStudent_Staff(0);
            int testMatric = -1;
            if (student >= 0 &&
                student <= students_staffs.Count)
            {
                List<string> studentDetails = students_staffs[student];
                testMatric = int.Parse(studentDetails[0]);
            }
            mod = new module();
            return mod.enroleUpdate(moduleCode, testMatric, mark);
        }

        /// <summary>
        /// This method SELECTs all student or staff data from the database, ordered by surname.
        /// </summary>
        /// <param name="student_staff">0 for student, 1 for staff</param>
        /// <returns>Nested List of strings containing all student or staff data</returns>
        public List<List<string>> existingStudent_Staff(int student_staff)
        {
            List<List<string>> students_staffs = new List<List<string>>();
            string tablename = "";
            string matric_payroll = "";
            switch (student_staff)
            {
                case 0:
                    tablename = "student";
                    matric_payroll = "matric";
                    break;
                case 1:
                    tablename = "staff";
                    matric_payroll = "payroll";
                    break;
                default:
                    students_staffs = null;
                    return students_staffs;
            }
            int counter = 0;
            SqlConnection con = connection.con;
            SqlCommand com = new SqlCommand("SELECT * FROM " + tablename + " ORDER BY lname, fname;", con);
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occured while reading from the database during the " +
                    "existingStudent_Staff method. Please close the program and rerun it.\r\n" +
                    "If the problem persists open Task Manager and close the sqlservr.exe process.", ex);
                students_staffs.Add(new List<string>());
                students_staffs[0].Add("An error occured while reading from the database during the " +
                    "existingStudent_Staff method.\r\n Please close the program and rerun it.\r\n" +
                    "If the problem persists open Task Manager and close the sqlservr.exe process.");
                students_staffs[0].Add("");
                students_staffs[0].Add("");
                return students_staffs;
            }
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                students_staffs.Add(new List<string>());
                students_staffs[counter].Add(sdr[matric_payroll].ToString());
                students_staffs[counter].Add(sdr["fname"].ToString());
                students_staffs[counter].Add(sdr["lname"].ToString());
                students_staffs[counter].Add(sdr["address"].ToString());
                students_staffs[counter].Add(sdr["email"].ToString());
                if (student_staff == 1)
                {
                    students_staffs[counter].Add(sdr["dept"].ToString());
                    students_staffs[counter].Add(sdr["role"].ToString());
                }
                counter++;
            }
            con.Close();
            return students_staffs;
        }

        /// <summary>
        /// This method SELECTs all module data from the database, ordered by name.
        /// </summary>
        /// <returns>Nested List of strings containing all module data</returns>
        public List<List<string>> existingModules()
        {
            int counter = 0;
            SqlConnection con = connection.con;
            List<List<string>> modules = new List<List<string>>();
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occured while reading from the database during the " +
                    "existingModules method. Please close the program and rerun it.\r\n" +
                    "If the problem persists open Task Manager and close the sqlservr.exe process.", ex);
                modules.Add(new List<string>());
                modules[0].Add("An error occured while reading from the database during the " +
                    "existingModules method.\r\n Please close the program and rerun it.\r\n" +
                    "If the problem persists open Task Manager and close the sqlservr.exe process.");
                modules[0].Add("");
                modules[0].Add("");
                return modules;
            }
            SqlCommand com = new SqlCommand("SELECT * FROM module ORDER BY name;", con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                modules.Add(new List<string>());
                modules[counter].Add(sdr["code"].ToString());
                modules[counter].Add(sdr["name"].ToString());
                modules[counter].Add(sdr["leader"].ToString());
                counter++;
            }
            con.Close();
            return modules;
        }

        /// <summary>
        /// This method SELECTs all students where the moduleCode inthe database matches 
        /// the given variable 'moduleCode.'
        /// </summary>
        /// <param name="moduleCode">module code</param>
        /// <returns>Nested List of strings containing students</returns>
        public List<List<string>> existingEnroled(string moduleCode)
        {
            moduleCode = stringFormat.formatForSQL(moduleCode);
            if (moduleCode != null &&
                moduleCode != "" &&
                moduleCode.Length <= 8)
            {
                int counter = 0;
                SqlConnection con = connection.con;
                List<List<string>> enroled = new List<List<string>>();
                try
                {
                    con.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error occured while reading from the database during the " +
                        "existingEnroled method. Please close the program and rerun it.\r\n" +
                        "If the problem persists open Task Manager and close the sqlservr.exe process.", ex);
                    enroled.Add(new List<string>());
                    enroled[0].Add("An error occured while reading from the database during the " +
                        "existingEnroled method.\r\n Please close the program and rerun it.\r\n" +
                        "If the problem persists open Task Manager and close the sqlservr.exe process.");
                    enroled[0].Add("");
                    enroled[0].Add("");
                    return enroled;
                }
                SqlCommand com = new SqlCommand("SELECT * FROM enrole WHERE moduleCode = @param0;", con);
                com.Parameters.AddWithValue("@param0", moduleCode);
                SqlDataReader sdr = com.ExecuteReader();
                while (sdr.Read())
                {
                    enroled.Add(new List<string>());
                    enroled[counter].Add(sdr["matric"].ToString());
                    enroled[counter].Add(sdr["mark"].ToString());
                    enroled[counter].Add(sdr["status"].ToString());
                    counter++;
                }
                con.Close();
                return enroled;
            }
            return null;
        }

        /// <summary>
        /// This method DELETEs one row from the database which contains the input
        /// moduleCode and matriculation number
        /// </summary>
        /// <param name="moduleCode">module code</param>
        /// <param name="matric">student matriculation number</param>
        /// <returns>boolean true if succesfully removed, false if unsuccesfully removed</returns>
        public bool removeEnrolled(string moduleCode, int matric)
        {
            moduleCode = stringFormat.formatForSQL(moduleCode);
            if (moduleCode != null &&
                moduleCode != "" &&
                moduleCode.Length <= 8 &&
                matric >= 1000 && matric <= 9000)
            {
                SqlConnection con = connection.con;
                SqlCommand com = con.CreateCommand();
                com.CommandText = "DELETE enrole WHERE moduleCode = @param0 AND matric = @param1";
                com.Parameters.AddWithValue("@param0", moduleCode);
                com.Parameters.AddWithValue("@param1", matric);
                com.Connection = con;
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                com.Transaction = tran;
                try
                {
                    com.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error occured while executing university.removeEnrolled.\r\n" +
                                "moduleCode was: " + moduleCode +
                                "matric was: " + matric +
                                "The transaction was not commited", ex);
                    tran.Rollback();
                    con.Close();
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// This method tests if there is a row in the database that contains the
        /// input moduleCode and matriculation number
        /// </summary>
        /// <param name="moduleCode">module code</param>
        /// <param name="matric">student matriculation number</param>
        /// <returns>boolean true is the row exists, false if not</returns>
        public bool alreadyEnrolled(string moduleCode, int matric)
        {
            moduleCode = stringFormat.formatForSQL(moduleCode);
            if (moduleCode != null &&
                moduleCode != "" &&
                moduleCode.Length <= 8 &&
                matric >= 1000 && matric <= 9000)
            {
                SqlConnection con = connection.con;
                SqlCommand com = con.CreateCommand();
                com.CommandText = "SELECT count(1) FROM enrole WHERE moduleCode = @param0 AND matric = @param1";
                com.Parameters.AddWithValue("@param0", moduleCode);
                com.Parameters.AddWithValue("@param1", matric);
                com.Connection = con;
                con.Open();
                int counter = (int)com.ExecuteScalar();
                con.Close();
                if (counter == 1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}