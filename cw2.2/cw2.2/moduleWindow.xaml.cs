﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw2
{
    /// <summary>
    /// Written by 40129963
    /// Nov-Dec 2014
    /// </summary>
    public partial class moduleWindow : Window
    {
        private university n = university.getInstance();
        private TextBlock[] textBlocks;
        private List<List<string>> modules;
        private List<List<string>> students;
        private List<List<string>> staff;
        private bool updateModule = false;

        /// <summary>
        /// This method initializes the window, and populates both cmbobxModule with all existing
        /// modules, and cmbobxLeader with all existing staff members.
        /// </summary>
        public moduleWindow()
        {
            InitializeComponent();
            textBlocks = new TextBlock[] { txtblkCode, txtblkName, txtblkLeader };
            modules = n.existingModules();
            switch (modules.Count())
            {
                case 0:
                    cmbobxModule.IsEnabled = false;
                    break;
                default:
                    foreach (List<String> l in modules)
                    {
                        cmbobxModule.Items.Add(l[0] + ": " + l[1]);
                    }
                    break;
            }
            students = n.existingStudent_Staff(0);
            if (students.Count() != 0)
            {
                cmbobxStudent_Populate(null);
                cmbobxStudent.IsEnabled = false;
            }
            staff = n.existingStudent_Staff(1);
            switch (staff.Count())
            {
                case 0:
                    cmbobxLeader.IsEnabled = false;
                    break;
                default:
                    foreach (List<string> l in staff)
                    {
                        cmbobxLeader.Items.Add(l[0] + ": " + l[2] + ", " + l[1]);
                    }
                    break;
            }
        }

        /// <summary>
        /// 1) If sender is btnSearchCode, the text of txtbxCode is checked to be valid
        ///    and then passed to the updateWindow method.
        /// 2) If sender is btnQuit, the window is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Quit_Click(object sender, RoutedEventArgs e)
        {
            if (sender == btnSearchCode)
            {
                string code = stringFormat.formatForSQL(txtbxCode.Text);
                if (code != "" && code != null && code.Length <= 8)
                {
                    updateWindow(code);
                }
                else
                {
                    txtblkError.Foreground = Brushes.Red;
                    txtblkError.Text = "Module Codes must be positive numbers. Please enter a new number.";
                }
            }
            else if (sender == btnQuit)
            {
                this.Close();
            }
        }

        /// <summary>
        /// This method uses the index of cmbobxLeader to pass the correct index to the method
        /// createUpdateModule to assign the correct staff member as Leader.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_Update_Click(object sender, RoutedEventArgs e)
        {
            int index = -1;
            if (cmbobxLeader.SelectedIndex >= 0)
            {
                index = cmbobxLeader.SelectedIndex;
            }
            int counter = 0;
            bool[] validation = n.createUpdateModule(txtbxCode.Text.ToString(), txtbxName.Text.ToString(), index);
            // The boolean Array vaidation corresponds to the textblocks in the textBlocks array,
            // so any boolean returned false marks the textblock foreground as red.
            // Each true boolean also increments the integer counter.
            for (int i = 0; i < validation.Length; i++)
            {
                if (validation[i])
                {
                    textBlocks[i].Foreground = Brushes.Black;
                    counter++;
                }
                else
                {
                    textBlocks[i].Foreground = Brushes.Red;
                }
            }
            // If counter is not the same value as the length of validation, then an error message
            // is displayed as at least one value was not valid.
            if (counter != validation.Length)
            {
                txtblkError.Foreground = Brushes.Red;
                txtblkError.Text = "Some data input wasn't valid; please check those marked in red.";
            }
            else
            {
                txtblkError.Foreground = Brushes.Black;
                txtblkError.Text = "Thank you,\r\n    " + txtbxName.Text.ToString() + " has been saved.";
                cmbobxStudent.IsEnabled = true;
            }
        }

        /// <summary>
        /// This method forces the user to search each module code they enter,
        /// so they can't try to commit invalid data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtbxName.IsEnabled = false;
            cmbobxLeader.IsEnabled = false;
            btnCreate_Update.IsEnabled = false;
            cmbobxStudent.IsEnabled = false;
            txtbxName.IsEnabled = false;
            btnEnrole_Update.IsEnabled = false;
            txtblkError.Foreground = Brushes.Black;
            txtblkError.Text = "Please use the search button to check the new Module Code is valid.";
        }

        /// <summary>
        /// 1) If sender is cmbobxModule the module code is chosen via the selectedIndex, and
        ///    that is passed to the updateWindow method
        /// 2) If sender is xmbobxStudent the student matriculation number is chosen
        ///    via the selectedIndex, and, as long as the module code and the selectedIndex
        ///    are both valid, they are passed to the alreadyEnrolled method. That method returns
        ///    boolean which is used to indicate if the selected student is already enrolled, in
        ///    which case btnEnrole_Update content is set to Remove, otherwise it is set to 
        ///    enroll
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbobx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == cmbobxModule)
            {
                if (cmbobxModule.SelectedIndex >= 0 &&
                    cmbobxModule.SelectedIndex <= modules.Count)
                {
                    List<string> chosen = modules[cmbobxModule.SelectedIndex];
                    updateWindow(chosen[0]);
                }
            }
            else if (sender == cmbobxStudent)
            {
                if (cmbobxStudent.SelectedIndex >= 0 &&
                    cmbobxStudent.SelectedIndex <= students.Count &&
                    txtbxCode.Text != null &&
                    txtbxCode.Text != "" &&
                    txtbxCode.Text.Length <= 8)
                {
                    List<string> student = students[cmbobxStudent.SelectedIndex];
                    if (n.alreadyEnrolled(txtbxCode.Text, int.Parse(student[0])))
                    {
                        btnRemove.IsEnabled = true;
                        btnEnrole_Update.Content = "Update";
                    }
                    else
                    {
                        btnRemove.IsEnabled = false;
                        btnEnrole_Update.Content = "Enroll";
                    }
                    btnEnrole_Update.IsEnabled = true;
                    txtbxMark.IsEnabled = true;
                }
            }
        }

        /// <summary>
        /// This method is passed a module code and checks the modules List to see if that code
        /// is already in use. If so the window is updated with the existing information about
        /// that module, stored in the database, otherwise the textboxes are set to empty strings.
        /// </summary>
        /// <param name="code">The chosen Module code</param>
        private void updateWindow(string code)
        {
            updateModule = false;
            int counter = 0;
            foreach (List<string> l in modules)
            {
                if (code.Equals(l[0]))
                {
                    updateModule = true;
                    btnCreate_Update.Content = "Update";
                    txtbxCode.Text = l[0];
                    txtbxName.Text = l[1];
                    foreach (List<string> s in staff)
                    {
                        if (s[0].Equals(l[2]))
                        {
                            cmbobxLeader.SelectedIndex = counter;
                        }
                        counter++;
                    }
                    cmbobxStudent_Populate(code);
                }
            }
            if (!updateModule)
            {
                btnCreate_Update.Content = "Create";
                txtbxName.Text = "";
                cmbobxLeader.SelectedIndex = -1;
            }
            txtblkError.Text = "";
            txtbxName.IsEnabled = true;
            cmbobxLeader.IsEnabled = true;
            btnCreate_Update.IsEnabled = true;
            cmbobxStudent.SelectedIndex = -1;
        }

        /// <summary>
        /// 1) If sender is btnEnrole_Update the students details are passed to the enroleUpdate 
        ///    method. If txtbxMark is empty, then no mark is passed and the student will be
        ///    listed as studying, otherwise the mark is passed as well.
        /// 2) If sender is btnRemove, the student details are passed to the removeEnrolled
        ///    method which returns a bool. If true, a message is displayed confirming the 
        ///    students removal, otherwise an error message is displayed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnrole_Remove_Update_Click(object sender, RoutedEventArgs e)
        {
            if (sender == btnEnrole_Update)
            {
                bool[] validation;
                int testMark = -1;
                if (!txtbxMark.Text.ToString().Equals(""))
                {
                    int.TryParse(txtbxMark.Text.ToString(), out testMark);
                }
                switch (testMark)
                {
                    case -1:
                        validation = n.enroleUpdate(txtbxCode.Text.ToString(), cmbobxStudent.SelectedIndex);
                        break;
                    default:
                        validation = n.enroleUpdate(txtbxCode.Text.ToString(), cmbobxStudent.SelectedIndex, testMark);
                        break;
                }
                if (!validation[2] &&
                    testMark != -1)
                {
                    txtblkMark.Foreground = Brushes.Red;
                    txtblkError.Foreground = Brushes.Red;
                    txtblkError.Text = "The mark you entered is not valid. Marks must be entered as a percentage, " +
                    "between 0 and 100 inclusive. \r\nIf the student hasn't been marked yet, simply leave the box empty.";
                }
                else
                {
                    txtblkMark.Foreground = Brushes.Black;
                    txtblkError.Foreground = Brushes.Black;
                    txtblkError.Text = "Thank you, the information has been saved to the database.";
                }
                btnEnrole_Update.Content = "Update";
                btnRemove.IsEnabled = true;
            }
            else if (sender == btnRemove)
            {
                if (cmbobxStudent.SelectedIndex >= 0 &&
                    cmbobxStudent.SelectedIndex <= students.Count)
                {
                    List<string> studentToRemove = students[cmbobxStudent.SelectedIndex];
                    if (n.removeEnrolled(txtbxCode.Text.ToString(), int.Parse(studentToRemove[0])))
                    {
                        txtblkError.Foreground = Brushes.Black;
                        txtblkError.Text = studentToRemove[1] + " " + studentToRemove[2] + " has been removed from the chosen module.";
                        btnRemove.IsEnabled = false;
                    }
                    else
                    {
                        txtblkError.Foreground = Brushes.Red;
                        txtblkError.Text = studentToRemove[1] + " " + studentToRemove[2] + " has not been removed from the selected module, " +
                            "please refresh the page and try again.";
                    }
                }
            }
        }

        /// <summary>
        /// This method populates cmbobxStudent with all students; those already enrolled on
        /// the chosen module (as denoted by the input variable, moduleCode) are set to black
        /// and those not enrolled are set to gray, so the user can easily distinguish them.
        /// </summary>
        /// <param name="moduleCode">The chosen Module code</param>
        private void cmbobxStudent_Populate(string moduleCode)
        {
            cmbobxStudent.Items.Clear();
            switch (students.Count())
            {
                case 0:
                    cmbobxStudent.IsEnabled = false;
                    break;
                default:
                    cmbobxStudent.IsEnabled = true;
                    foreach (List<string> l in students)
                    {
                        ComboBoxItem student = new ComboBoxItem();
                        if (n.alreadyEnrolled(moduleCode, int.Parse(l[0])))
                        {
                            student.Foreground = Brushes.Black;
                        }
                        else
                        {
                            student.Foreground = Brushes.Gray;
                        }
                        student.Content = (l[0] + ": " + l[2] + ", " + l[1]);
                        cmbobxStudent.Items.Add(student);
                    }
                    break;
            }
        }
    }
}
