﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace cw2
{
    /// <summary>
    /// Written by 40129963
    /// Nov-Dec 2014
    /// </summary>
    static class connection
    {
        /// <summary>
        /// This private method establishes a database connection, based on the current working directoy.
        /// The path is tailored to work for either the main program or the test methods.
        /// </summary>
        /// <returns>working connection path</returns>
        private static SqlConnection privCon()
        {
            string pwd = System.IO.Directory.GetCurrentDirectory();

            if (pwd.Contains(@"\cw2_unitTests"))
            {
                pwd = string.Concat(pwd.Substring(0, pwd.IndexOf(@"\cw2_unitTests")), @"\cw2.2");
            }
            else
            {
                for (int i = 0; i < 2; i++)
                {
                    int end = pwd.LastIndexOf(@"\");
                    pwd = pwd.Substring(0, end);
                }
            }
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + pwd + @"\university_database.mdf;Integrated Security=True;Connect Timeout=30");
            return con;
        }

        /// <summary>
        /// Public getter for privCon() method
        /// </summary>
        public static SqlConnection con { get { return privCon(); } }
    }
}
