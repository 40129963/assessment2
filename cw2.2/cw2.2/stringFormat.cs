﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
    /// <summary>
    /// Written by 40129963
    /// Nov-Dec 2014
    /// </summary>
    public static class stringFormat
    {
        /// <summary>
        /// This method adds a second apostrophe after all apostrophes found in the given string.
        /// Using a single apostrophe in an SQL statement denotes a literal string and can cause
        /// errors, so this avoids those errors.
        /// If no apostrophes are present, the string remains unchanged.
        /// </summary>
        /// <param name="entity">String to be used in SQL statement</param>
        /// <returns>String with single apostrophes replaced with double</returns>
        public static string formatForSQL(string entity)
        {
            if (entity != null && entity != "")
            {
                int j = 1;
                int start = 0;
                int index = 0;
                string beforeApo, afterApo;
                for (int i = 0; i < j; i++)
                {
                    if (entity.Substring(start, entity.Length - start - 1).Contains("'"))
                    {
                        index = entity.Substring(start, entity.Length - start - 1).IndexOf("'");
                        beforeApo = entity.Substring(0, start + index);
                        afterApo = entity.Substring(start + index, entity.Length - index - start);
                        entity = string.Concat(string.Concat(beforeApo, "'"), afterApo);
                        start += index + 2;
                        j++;
                    }
                }
                if (entity.LastIndexOf("'") == entity.Length - 1)
                {
                    entity = string.Concat(entity, "'");
                }
            }
            return entity;
        }
    }
}
