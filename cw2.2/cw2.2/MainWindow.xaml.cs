﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace cw2
{
    /// <summary>
    /// Written by 40129963
    /// Nov-Dec 2014
    /// </summary>
    public partial class MainWindow : Window
    {
        private university n = university.getInstance();
        private List<List<string>> student_staffList;
        private List<List<string>> modules;
        private List<List<string>> enroled;
        public MainWindow() { InitializeComponent(); }

        /// <summary>
        /// 1) If sender is cmbobxSSM, the method will enable the confirm button
        /// 2) If sender is cmbobxReview, the method will do different things
        ///    depending on cmbobxReview's selectedIndex:
        ///    0-2) review button is enabled
        ///    3) lstbxReview is enabled and populated
        ///    >3) Error message displayed
        /// 3) If sender is cmbobxMod, the method will enable the review button
        /// default) Error message is displayed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbobx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == cmbobxSSM)
            {
                btnConfirm.IsEnabled = true;
            }
            else if (sender == cmbobxReview)
            {
                if (cmbobxReview.SelectedIndex >= 0 && cmbobxReview.SelectedIndex < 3)
                {
                    btnReview.IsEnabled = true;
                    cmbobxMod.IsEnabled = false;
                    cmbobxMod.SelectedIndex = -1;
                }
                else if (cmbobxReview.SelectedIndex == 3)
                {
                    btnReview.IsEnabled = false;
                    cmbobxMod.IsEnabled = true;
                    cmbobxMod.Items.Clear();
                    List<List<string>> modules = n.existingModules();
                    switch (modules.Count)
                    {
                        case 0:
                            lstbxReview.Items.Add("There are no module details to view.");
                            break;
                        default:
                            foreach (List<string> l in modules)
                            {
                                cmbobxMod.Items.Add(l[0] + ": " + l[1]);
                            }
                            break;
                    }
                }
                else
                {
                    txtblkError.Text = "An error has occured, please make a new selection.";
                }
            }
            else if (sender == cmbobxMod)
            {
                btnReview.IsEnabled = true;
            }
            else
            {
                txtblkError.Text = "An error has occured, please make a new selection.";
            }
        }

        /// <summary>
        /// 1) If sender is btnReview, the method will do different things
        ///    depending on cmbobxReview's selectedIndex:
        ///    0) lstbxReview is populated with all student details
        ///    1) lstbxReview is populated with all staff details
        ///    2) lstbxReview is populated with all module details
        ///    3) lstbxReview is populated with all students enroled on the selected 
        ///       module, as indicated by the selected index of cmbobxMod
        ///    >3) Error message displayed
        /// 2) If sender is btnConfirm, the method will open a different window
        ///    depending on cmbobxSSM's selectedIndex:
        ///    0) student_staffWindow is opened for student details
        ///    1) student_staffWindow is opened for staff details
        ///    2) moduleWindow is opened
        ///    >2) Error message displayed
        /// 3) If sender is btnQuit mainWindow is closed
        /// default) Error message is displayed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            if (sender == btnReview)
            {
                lstbxReview.Items.Clear();
                switch (cmbobxReview.SelectedIndex)
                {
                    case 0:
                    case 1:
                        student_staffList = n.existingStudent_Staff(cmbobxReview.SelectedIndex);
                        if (student_staffList.Count == 0)
                        {
                            lstbxReview.Items.Add("There are no records to view");
                        }
                        else
                        {
                            foreach (List<string> l in student_staffList)
                            {
                                lstbxReview.Items.Add(l[0] + ": " + l[2] + ", " + l[1]);
                            }
                        }
                        break;
                    case 2:
                        student_staffList = n.existingStudent_Staff(1);
                        modules = n.existingModules();
                        if (modules.Count == 0)
                        {
                            lstbxReview.Items.Add("There are no records to view");
                        }
                        else
                        {
                            string leader = null;
                            foreach (List<string> l in modules)
                            {
                                foreach (List<string> staff in student_staffList)
                                {
                                    if (staff[0].Equals(l[2]))
                                    {
                                        leader = " - Leader: " + staff[0] + ": " + staff[2] + ", " + staff[1];
                                    }
                                }
                                string listThis = string.Format("{0,-5}: {1,-50}" + leader, l[0], l[1]);
                                lstbxReview.Items.Add(listThis);
                            }
                        }
                        break;
                    case 3:
                        // The correct module is selected from the existingModules method by
                        // using the selectedIndex of cmbobxMod.
                        List<string> module = n.existingModules()[cmbobxMod.SelectedIndex];
                        // enroled is populated by using the module code from module, selected
                        // in the previous line
                        enroled = n.existingEnroled(module[0]);
                        student_staffList = n.existingStudent_Staff(0);
                        switch (enroled.Count)
                        {
                            case 0:
                                lstbxReview.Items.Add("There are no students enroled on this module");
                                break;
                            default:
                                foreach (List<string> s in student_staffList)
                                {
                                    foreach (List<string> l in enroled)
                                    {
                                        if (l[0].Equals(s[0]))
                                        {
                                            // When the matriculation number from student_staffList matches a matriculation number in enroled,
                                            // the details of the student are retrieved from student_staffList
                                            string listThis = string.Format(s[0] + ": {0,-42} - {1,-10} - " + l[1], string.Concat(string.Concat(s[2], ", "), s[1]), l[2]);
                                            lstbxReview.Items.Add(listThis);
                                            break;
                                        }
                                    }
                                }
                                break;
                        }
                        break;
                    default:
                        txtblkError.Text = "An error has occured, please make a new selection.";
                        break;
                }
            }
            else if (sender == btnConfirm)
            {
                Window win = null;
                switch (cmbobxSSM.SelectedIndex)
                {
                    case 0:
                    case 1:
                        win = new student_staffWindow(cmbobxSSM.SelectedIndex);
                        break;
                    case 2:
                        win = new moduleWindow();
                        break;
                    default:
                        txtblkError.Text = "An error has occured, please make a new selection.";
                        break;
                }
                win.ShowDialog();
            }
            else if (sender == btnQuit)
            {
                this.Close();
            }
            else
            {
                txtblkError.Text = "An error has occured, please make a new selection.";
            }
        }
    }
}
