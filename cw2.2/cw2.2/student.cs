﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace cw2
{
    /// <summary>
    /// Written by 40129963
    /// Nov-Dec 2014
    /// 
    /// There are no getters or setters for this class, as at no point in the code should 
    /// any attributes be accessed without going through an existing method.
    /// </summary>
    public class student
    {
        private int privMatric;
        private string privFname;
        private string privLname;
        private string privAddress;
        private string privEmail;

        /// <summary>
        /// This method tests all input variables; if they are all valid the matriculation number 
        /// is used to test if the student already exists in the database. If so, an UPDATE 
        /// transaction is used, otherwise a CREATE transaction is used.
        /// </summary>
        /// <param name="matric">Matriculation number between 1000 and 9000</param>
        /// <param name="fname">student's first name</param>
        /// <param name="lname">student's last name</param>
        /// <param name="address">student's address</param>
        /// <param name="email">student's email</param>
        /// <returns>boolean Array indicating validity of each attribute</returns>
        public bool[] createUpdate(int matric, string fname, string lname, string address, string email)
        {
            // All strings are passed through the static formatForSQL  method, 
            // to fix any apostrophes
            fname = stringFormat.formatForSQL(fname);
            lname = stringFormat.formatForSQL(lname);
            address = stringFormat.formatForSQL(address);
            email = stringFormat.formatForSQL(email);
            bool[] validation = { false, false, false, false, false };
            int validationCount = 0;
            if (matric >= 1000 && matric <= 9000)
            {
                privMatric = matric;
                validation[0] = true;
                validationCount++;
            }
            if (fname != null && fname != "" && fname.Length <= 20)
            {
                privFname = fname;
                validation[1] = true;
                validationCount++;
            }
            if (lname != null && lname != "" && lname.Length <= 20)
            {
                privLname = lname;
                validation[2] = true;
                validationCount++;
            }
            if (address != null && address != "" && address.Length <= 100)
            {
                privAddress = address;
                validation[3] = true;
                validationCount++;
            }
            if (email != null && email.Contains("@") && email.Length <= 100)
            {
                privEmail = email;
                validation[4] = true;
                validationCount++;
            }
            //if every element in the array is true, then validationCount will be the same value as the length of validation
            if (validationCount == validation.Length)
            {
                {
                    SqlConnection con = connection.con;
                    con.Open();
                    SqlCommand com = con.CreateCommand();
                    com.CommandText = "SELECT count(1) FROM student WHERE matric = @param1";
                    com.Parameters.AddWithValue("@param1", privMatric);
                    com.Connection = con;
                    int counter = (int)com.ExecuteScalar();
                    SqlTransaction tran = con.BeginTransaction();
                    com.Transaction = tran;
                    switch (counter)
                    {
                        case 0:
                            com.CommandText = "INSERT INTO student (matric, fname,lname,address,email) VALUES (@param1,@param2,@param3,@param4,@param5)";
                            break;
                        case 1:
                            com.CommandText = "UPDATE student SET fname = @param2, lname = @param3 ,address = @param4 ,email = @param5 WHERE (matric = @param1)";
                            break;
                        default:
                            validation = new bool[] { false, false, false, false, false };
                            return validation;
                    }
                    com.Parameters.AddWithValue("@param2", privFname);
                    com.Parameters.AddWithValue("@param3", privLname);
                    com.Parameters.AddWithValue("@param4", privAddress);
                    com.Parameters.AddWithValue("@param5", privEmail);
                    try
                    {
                        com.ExecuteNonQuery();
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("An error occured while executing student.createUpdate." +
                            "The transaction was not commited", ex);
                        tran.Rollback();
                    }
                    con.Close();
                }
            }
            return validation;
        }
    }
}
