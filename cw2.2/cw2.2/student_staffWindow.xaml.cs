﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw2
{
    /// <summary>
    /// Written by 40129963
    /// Nov-Dec 2014
    /// 
    /// student_staffWindow is created as either the student or the staff window depending on the
    /// mainWindowChoice integer passed to the initializer method
    /// </summary>
    public partial class student_staffWindow : Window
    {
        private university n = university.getInstance();
        // student_staff is either 0 or 1 and is used throughout the class to mark whether the 
        // window is addressing students or staff. 
        private int student_staff;
        private List<List<string>> student_staffList;
        private bool updateStudent_Staff = false;
        private TextBlock[] textBlocks;

        /// <summary>
        /// Initialises window, and sets it to display either the student or staff choices, depending on the index given from
        /// MainWindow. Also saves student_staff as a global variable to match that selection.
        /// student_staffList is also populated upon Window initialization.
        /// </summary>
        /// <param name="mainWindowChoice"></param>
        public student_staffWindow(int mainWindowChoice)
        {
            InitializeComponent();
            textBlocks = new TextBlock[] { txtblkMatric_Payroll, txtblkName, txtblkName, txtblkAddress, txtblkEmail, txtblkDept, txtblkRole };
            student_staff = mainWindowChoice;
            switch (student_staff)
            {
                case 0:
                    // Case 0 shows the student information
                    Title = "Update / Add Student";
                    txtblkMatric_Payroll.Text = "Matriculation Number";
                    txtblkEnterName.Text = "Search by existing Student Name";
                    txtblkEnterMatric.Text = "Search by Matriculation Number";
                    txtblkEnterInfo.Text = "     Please select the name of the Student from " +
                        "the drop down menu, to edit their details, or enter a Matriculation " +
                        "Number.\r\n     If the Number is assigned to a student, you will be able to " +
                        "edit that students details, otherwise you will be able to enter the " +
                        "new students information.";
                    txtblkDept.Visibility = System.Windows.Visibility.Hidden;
                    txtbxDept.Visibility = System.Windows.Visibility.Hidden;
                    txtblkRole.Visibility = System.Windows.Visibility.Hidden;
                    cmbobxRole.Visibility = System.Windows.Visibility.Hidden;
                    break;
                case 1:
                    // Case 1 shows the staff information 
                    Title = "Update / Add Staff";
                    txtblkMatric_Payroll.Text = "Payroll Number";
                    txtblkEnterName.Text = "Search by existing Staff Name";
                    txtblkEnterMatric.Text = "Search by Payroll Number";
                    txtblkEnterInfo.Text = "     Please select the name of the Staff Member from " +
                        "the drop down menu, to edit their details, or enter a Payroll Number." +
                        "\r\n     If the Number is assigned to a Staff Member, you will be able to " +
                        "edit that Members details, otherwise you will be able to enter the " +
                        "new Members information.";
                    txtblkDept.Visibility = System.Windows.Visibility.Visible;
                    txtbxDept.Visibility = System.Windows.Visibility.Visible;
                    txtblkRole.Visibility = System.Windows.Visibility.Visible;
                    cmbobxRole.Visibility = System.Windows.Visibility.Visible;
                    break;
                default:
                    // Case of neither 0 or 1 will display an error message and close the window
                    System.Windows.Forms.MessageBox.Show("An error occured, please try re-opening the window.");
                    this.Close();
                    break;
            }
            student_staffList = n.existingStudent_Staff(student_staff);
            // If there are no entries in the list, cmbobxStudent_Staff is not enabled
            if (student_staffList.Count() == 0)
            {
                cmbobxStudent_Staff.IsEnabled = false;
            }
            else
            {
                foreach (List<string> l in student_staffList)
                {
                    cmbobxStudent_Staff.Items.Add(l[0] + ": " + l[2] + ", " + l[1]);
                }
            }
        }

        /// <summary>
        /// This method selects the relevant student or staff member from the student_staffList and passes
        /// it to the updateWindow as long as the selectedIndex is valid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbobxStudent_Staff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbobxStudent_Staff.SelectedIndex >= 0 &&
                cmbobxStudent_Staff.SelectedIndex <= student_staffList.Count)
            {
                List<string> chosen = student_staffList[cmbobxStudent_Staff.SelectedIndex];
                updateWindow(int.Parse(chosen[0]));
            }
        }

        /// <summary>
        /// When the text is changed the user is forced to check if the new number is valid, 
        /// rather than leaving the form fully enabled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtbxMatric_Payroll_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnCreateUpdate.IsEnabled = false;
            txtbxFname.IsEnabled = false;
            txtbxLname.IsEnabled = false;
            txtbxAddress.IsEnabled = false;
            txtbxEmail.IsEnabled = false;
            string numberName = "";
            if (student_staff == 0)
            {
                numberName = "Matriculation";
            }
            else
                if (student_staff == 1)
                {
                    txtbxDept.IsEnabled = false;
                    cmbobxRole.IsEnabled = false;
                    numberName = "Payroll";
                }
            txtblkError.Foreground = Brushes.Black;
            txtblkError.Text = "Please use the search button to check the new " + numberName + " Number is valid.";
        }

        /// <summary>
        /// 1) If sender is btnCreateUpdate, the input values of the user are checked and passed to
        ///    the student or staff class. Each method passes back an array of booleans, which are
        ///    used to mark whether any of the values passed were invalid, so the user can be
        ///    informed. If any boolean is false, the error message will show, otherwise a message
        ///    is displayed thanking the user and confirming the succesful creation or update of the
        ///    student or staff member
        /// 2) If sender is btnQuit, the window is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_Quit_Click(object sender, RoutedEventArgs e)
        {
            if (sender == btnCreateUpdate)
            {
                int counter = 0;
                string email = null;
                string roleString = null;
                ComboBoxItem roleItem = (ComboBoxItem)cmbobxRole.SelectedItem;
                if (cmbobxRole.SelectedIndex == 0 ||
                    cmbobxRole.SelectedIndex == 1 ||
                    cmbobxRole.SelectedIndex == 2)
                {
                    roleString = roleItem.Content.ToString();
                }
                else
                {
                    roleString = "error";
                }
                bool[] validation = n.createUpdateStudent_Staff(student_staff, int.Parse(txtbxMatric_Payroll.Text.ToString()), txtbxFname.Text.ToString(), txtbxLname.Text.ToString(), txtbxAddress.Text.ToString(), txtbxEmail.Text.ToString(), txtbxDept.Text.ToString(), roleString);
                // The for loop counts as high as validation.Count()-student_staff because the last element of the staff
                // validation doesn't use a textbox, so needs to be one shorter. students are marked as student_staff being 0
                // so nothing is reduced, but for staff 1 is reduced, to fix the textbox count issue.
                for (int i = 0; i < validation.Count() - student_staff; i++)
                {
                    if (validation[i])
                    {
                        counter++;
                        textBlocks[i].Foreground = Brushes.Black;
                    }
                    else
                    {
                        textBlocks[i].Foreground = Brushes.Red;
                        if (i == 4)
                        {
                            email = "An email address must contain an @ symbol.";
                        }
                    }
                }
                if (!validation[1] || !validation[2])
                {
                    textBlocks[1].Foreground = Brushes.Red;
                }
                int roleSelected = 0;
                if (student_staff == 1)
                {
                    if (validation[6] == true)
                    {
                        roleSelected = 1;
                        txtblkRole.Foreground = Brushes.Black;
                    }
                    else
                    {
                        txtblkRole.Foreground = Brushes.Red;
                    }
                }
                if (counter != validation.Count() - student_staff ||
                    roleSelected != student_staff)
                {
                    txtblkError.Foreground = Brushes.Red;
                    txtblkError.Text = "Some data input wasn't valid; please check those marked in red.\r\n" + email;
                }
                else
                {
                    txtblkError.Foreground = Brushes.Black;
                    txtblkError.Text = "Thank you,\r\n    " + txtbxFname.Text.ToString() + " " + txtbxLname.Text.ToString() + "'s records have been saved.";
                }
            }
            else if (sender == btnQuit)
            {
                this.Close();
            }
        }

        /// <summary>
        /// This method tries to convert the txtbxMatric_Payroll Text to an integer, and then check
        /// the number is within the validation limits as outlined in the coursework.
        /// If the number is valid, the method then passes the integer to the updateWindow method,
        /// otherwise an error message is displayed so the user can enter a new number.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchNumber_Click(object sender, RoutedEventArgs e)
        {
            bool goodNumber = false;
            int testMatric_Payroll = -1;
            int.TryParse(txtbxMatric_Payroll.Text, out testMatric_Payroll);
            // Tests if the input number is a valid value for either a Matriculation or payroll Number
            switch (student_staff)
            {
                case 0:
                    if (testMatric_Payroll >= 1000 &&
                        testMatric_Payroll <= 9000)
                    {
                        goodNumber = true;
                    }
                    break;
                case 1:
                    if (testMatric_Payroll >= 9000 &&
                        testMatric_Payroll <= 9999)
                    {
                        goodNumber = true;
                    }
                    break;
            }
            //Only if the number passes the check does the code enable the further detail textboxes
            if (goodNumber)
            {
                updateWindow(testMatric_Payroll);
            }
            else
            {
                string errorString = null;
                txtblkError.Foreground = Brushes.Red;
                switch (student_staff)
                {
                    case 0:
                        errorString = "Matriculation Numbers must be between 1000 and 9000 inclusive." +
                            " Please enter a new number.";
                        break;
                    case 1:
                        errorString = "Payroll Numbers must be between 9000 and 9999 inclusive." +
                            " Please enter a new number.";
                        break;
                }
                txtblkError.Text = errorString;
            }
        }

        /// <summary>
        /// This method uses the matriculation or payroll number to find the student or
        /// staff member with that number assigned, and populate the textboxes with the
        /// values. If no primary key matches the number, and the number is valid, 
        /// then the textboxes are enabled without Text in them.
        /// </summary>
        /// <param name="matric_payroll"></param>
        private void updateWindow(int matric_payroll)
        {
            if ((student_staff == 0 &&
                matric_payroll >= 1000 &&
                matric_payroll <= 9000)
                ||
                (student_staff == 1 &&
                matric_payroll >= 9000 &&
                matric_payroll <= 9999))
            {
                updateStudent_Staff = false;
                foreach (List<string> l in student_staffList)
                {
                    if (matric_payroll == int.Parse(l[0]))
                    {
                        // If a match is found, the boolean updateStudent_Staff is set to true. 
                        updateStudent_Staff = true;
                        btnCreateUpdate.Content = "Update";
                        txtbxMatric_Payroll.Text = l[0];
                        txtbxFname.Text = l[1];
                        txtbxLname.Text = l[2];
                        txtbxAddress.Text = l[3];
                        txtbxEmail.Text = l[4];
                        if (student_staff == 1)
                        {
                            txtbxDept.Text = l[5];
                            // The array of strings, roles, is populated with the three possible
                            // roles possessed by a staff member in the same order as displayed
                            // in the combobox. When the saved value matches a value in the 
                            // roles array, that matched index is used for the selectedIndex of
                            // cmbobxRole.
                            string[] roles = { "Lecturer", "Senior Lecturer", "Professor" };
                            for (int i = 0; i < roles.Length; i++)
                            {
                                if (l[6].Equals(roles[i]))
                                {
                                    cmbobxRole.SelectedIndex = i;
                                }
                            }
                        }
                    }
                }
                // If updateStudent_Staff wasn't changed to true it means the 
                // number wasn't matched to an existing primary key in the 
                // database
                if (!updateStudent_Staff)
                {
                    btnCreateUpdate.Content = "Create";
                    txtbxFname.Text = "";
                    txtbxLname.Text = "";
                    txtbxAddress.Text = "";
                    txtbxEmail.Text = "";
                    if (student_staff == 1)
                    {
                        txtbxDept.Text = "";
                        cmbobxRole.SelectedIndex = -1;
                    }
                }
                txtblkError.Text = null;
                txtbxFname.IsEnabled = true;
                txtbxLname.IsEnabled = true;
                txtbxAddress.IsEnabled = true;
                txtbxEmail.IsEnabled = true;
                if (student_staff == 1)
                {
                    txtbxDept.IsEnabled = true;
                    cmbobxRole.IsEnabled = true;
                }
                btnCreateUpdate.IsEnabled = true;

            }
        }
    }
}
