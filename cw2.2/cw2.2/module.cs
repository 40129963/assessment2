﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace cw2
{
    /// <summary>
    /// Written by 40129963
    /// Nov-Dec 2014
    /// 
    /// There are no getters or setters for this class, as at no point in the code should 
    /// any attributes be accessed without going through an existing method.
    /// </summary>
    public class module
    {
        private string privCode;
        private string privName;
        private int privLeader;

        /// <summary>
        /// This method tests all input variables; if they are all valid the code string 
        /// is used to test if the module already exists in the database. If so, an UPDATE 
        /// transaction is used, otherwise a CREATE transaction is used.
        /// The same process is used to ensure the leader variable is a payroll primary key
        /// already listed in the database.
        /// </summary>
        /// <param name="code">module code</param>
        /// <param name="name">module name</param>
        /// <param name="leader">payroll number of existing staff</param>
        /// <returns>boolean Array indicating validity of each attribute</returns>
        public bool[] createUpdate(string code, string name, int leader)
        {
            // All strings are passed through the static formatForSQL  method, 
            // to fix any apostrophes
            code = stringFormat.formatForSQL(code);
            name = stringFormat.formatForSQL(name);
            int validationCount = 0;
            bool[] validation = { false, false, false };
            if (code != "" && code != null && code.Length <= 8)
            {
                validation[0] = true;
                privCode = code;
                validationCount++;
            }
            if (name != "" && name != null && name.Length <= 50)
            {
                validation[1] = true;
                privName = name;
                validationCount++;
            }
            SqlConnection con = connection.con;
            SqlCommand com = con.CreateCommand();
            com.Connection = con;
            if (leader >= 9000 &&
                leader <= 9999)
            {
                com.CommandText = "SELECT count(1) FROM staff WHERE payroll = @param2";
                com.Parameters.AddWithValue("@param2", leader);
                con.Open();
                int countLeader = (int)com.ExecuteScalar();
                if (countLeader == 1)
                {
                    validation[2] = true;
                    privLeader = leader;
                    validationCount++;
                }
            }
            //if every element in the array is true, then validationCount will be the same value as the length of validation
            if (validationCount == 3)
            {
                com.CommandText = "SELECT count(1) FROM module WHERE code = @param0";
                com.Parameters.AddWithValue("@param0", privCode);
                int counter = (int)com.ExecuteScalar();
                SqlTransaction tran = con.BeginTransaction();
                com.Transaction = tran;
                switch (counter)
                {
                    case 0:
                        com.CommandText = "INSERT INTO module (code,name,leader) VALUES (@param0,@param1,@param2)";
                        break;
                    case 1:
                        com.CommandText = "UPDATE module SET name = @param1, leader = @param2 WHERE (code = @param0)";
                        break;
                    default:
                        validation = new bool[] { false, false, false };
                        return validation;
                }
                try
                {
                    com.Parameters.AddWithValue("@param1", privName);
                    com.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error occured while executing module.createUpdate." +
                            "The transaction was not commited", ex);
                    tran.Rollback();
                }
            }
            con.Close();
            return validation;
        }

        /// <summary>
        /// This method tests all input variables; if they are all valid the matric number 
        /// is used to test if the student is already enrolled on the module. If so, an UPDATE 
        /// transaction is used, otherwise a CREATE transaction is used.
        /// If a mark value was given, and is valid, the mark is assigned to the database
        /// at the same time, with the corresponding status. If no mark is given then the
        /// mark attribute remains null in the database and the status is set to studying:
        ///   Although the coursework specified the mark should be set as -1 if no mark was given,
        /// I felt this was a better use of space, and lessened the amount of redundant 
        /// data.
        /// </summary>
        /// <param name="moduleCode">module code</param>
        /// <param name="matric">matriculation number of student</param>
        /// <param name="mark">optional mark; defaults to -1</param>
        /// <returns>boolean Array indicating validity of each attribute</returns>
        public bool[] enroleUpdate(string moduleCode, int matric, int mark = -1)
        {
            moduleCode = stringFormat.formatForSQL(moduleCode);
            bool[] validation = { false, false, false, false };
            SqlConnection con = connection.con;
            con.Open();
            SqlCommand com = con.CreateCommand();
            com.Connection = con;
            com.CommandText = "SELECT count(1) FROM module WHERE code = @param0";
            com.Parameters.AddWithValue("@param0", moduleCode);
            int counterMod = (int)com.ExecuteScalar();
            if (counterMod == 1)
            {
                validation[0] = true;
            }
            com.CommandText = "SELECT count(1) FROM student WHERE matric = @param3";
            com.Parameters.AddWithValue("@param3", matric);
            int counterMatric = (int)com.ExecuteScalar();
            if (counterMatric == 1)
            {
                validation[3] = true;
            }
            string status = "Studying";
            if (mark >= 0 && mark < 40)
            {
                status = "Failed";
                validation[2] = true;
            }
            else if (mark >= 40 && mark <= 100)
            {
                status = "Passed";
                validation[2] = true;
            }
            if (validation[0] && validation[3])
            {
                com.CommandText = "SELECT count(1) FROM enrole WHERE moduleCode = @param0 AND matric = @param3";
                int counter = (int)com.ExecuteScalar();
                SqlTransaction tran = con.BeginTransaction();
                com.Transaction = tran;
                switch (counter)
                {
                    case 0:
                        switch (validation[2])
                        {
                            case true:
                                com.CommandText = "INSERT INTO enrole (moduleCode,mark,status,matric) VALUES (@param0,@param1,@param2,@param3)";
                                com.Parameters.AddWithValue("@param1", mark);
                                break;
                            default:
                                com.CommandText = "INSERT INTO enrole (moduleCode,status,matric) VALUES (@param0,@param2,@param3)";
                                break;
                        }
                        break;
                    case 1:
                        switch (validation[2])
                        {
                            case true:
                                com.CommandText = "UPDATE enrole SET moduleCode = @param0, mark = @param1, status = @param2, matric = @param3 WHERE (moduleCode = @param0 AND matric = @param3)";
                                com.Parameters.AddWithValue("@param1", mark);
                                break;
                            default:
                                com.CommandText = "UPDATE enrole SET moduleCode = @param0, status = @param2, matric = @param3 WHERE (moduleCode = @param0 AND matric = @param3)";
                                break;
                        }
                        break;
                    default:
                        validation = new bool[] { false, false, false, false };
                        return validation;
                }
                com.Parameters.AddWithValue("@param2", status);
                try
                {
                    com.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error occured while executing module.enroleUpdate." +
                            "The transaction was not commited", ex);
                    tran.Rollback();
                }
            }
            con.Close();
            return validation;
        }
    }
}