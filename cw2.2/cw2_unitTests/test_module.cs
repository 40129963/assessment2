﻿using cw2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;

namespace cw2_unitTests
{
    [TestClass]
    public class test_module
    {
        university n = university.getInstance();
        module m = new module();

        /// <summary>
        /// Tests the creation or update of a module, with valid variables
        /// </summary>
        [TestMethod]
        public void test_createUpdate_pass()
        {           
            // Arrange
            string moduleCode = "AAA11";
            string name = "Computing";
            List<string> existingLeader = n.existingStudent_Staff(1)[0];
            int leader = int.Parse(existingLeader[0]);
            bool[] expected = { true, true, true };

            // Act
            bool[] actual = m.createUpdate(moduleCode, name, leader);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Module created correctly.");
            } 
        }

        /// <summary>
        /// Tests the creation or update of a module, with an invalid leader choice, as
        /// their payroll number does not exist in the database
        /// </summary>
        [TestMethod]
        public void test_createUpdate_fail_a()
        {
            // Arrange
            string moduleCode = "AAA11";
            string name = "Computing";
            int leader = 9000;
            // Leader incraments until no staff member is assigned that value, to guarentee the desired result
            List<List<string>> leaders = n.existingStudent_Staff(1);
            int j = 1;
            bool exists; 
            for (int i = 0; i < j;i++ )
            {
                exists = false;
                foreach (List<string> l in leaders)
                {
                    if (int.Parse(l[0]) == leader)
                    {
                        exists = true;
                        break;
                    }
                }
                if (exists)
                {
                    leader++;
                    j++;
                }
            }
            bool[] expected = { true, true, false };

            // Act
            bool[] actual = m.createUpdate(moduleCode, name, leader);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Module not created as leader non-existant "+
                                                        "even though leader code is a valid value.");
            } 
        }

        /// <summary>
        /// Tests the creation or update of a module with invalid variables
        /// </summary>
        [TestMethod]
        public void test_createUpdate_fail_b()
        {
            // Arrange
            string moduleCode = "tooTooLong";
            string name = null;
            int leader = -1;
            bool[] expected = { false, false, false };

            // Act
            bool[] actual = m.createUpdate(moduleCode, name, leader);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Module not created as values invalid.");
            }
        }
    
        /// <summary>
        /// Tests the creation or update of a module with valid variables, but without a mark
        /// </summary>
        [TestMethod]
        public void test_enroleUpdate_pass_noMark()
        {
            // Arrange
            List<string> existingModule = n.existingModules()[0];
            string code = existingModule[0];
            List<string> existingStudent = n.existingStudent_Staff(0)[0];
            int matric = int.Parse(existingStudent[0]);
            bool[] expected = { true, false, false, true };

            // Act
            bool[] actual = m.enroleUpdate(code, matric);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Module created correctly.");
            } 
        }

        /// <summary>
        /// Tests the creation or update of a module with valid variables, including mark
        /// </summary>
        [TestMethod]
        public void test_enroleUpdate_pass_withMark()
        {
            // Arrange
            List<string> existingModule = n.existingModules()[0];
            string code = existingModule[0];
            List<string> existingStudent = n.existingStudent_Staff(0)[0];
            int matric = int.Parse(existingStudent[0]);
            int mark = 90;
            bool[] expected = { true, false, true, true };

            // Act
            bool[] actual = m.enroleUpdate(code, matric, mark);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Module created correctly.");
            }
        }
        
        /// <summary>
        /// Tests the enrolement of a student on a module, with an invalid matriculation number
        /// </summary>
        [TestMethod]
        public void test_enroleUpdate_fail()
        {
            // Arrange
            int codeInt = 1;
            List<List<string>> modules = n.existingModules();
            int j = 1;
            bool exists;
            for (int i = 0; i < j; i++)
            {
                exists = false;
                foreach (List<string> mo in modules)
                {
                    if (mo[0].Equals(codeInt.ToString()))
                    {
                        exists = true;
                        break;
                    }
                }
                if (exists)
                {
                    codeInt++;
                    j++;
                }
            }
            string code = codeInt.ToString();
            int matric = 1000;
            List<List<string>> students = n.existingStudent_Staff(0);
            j = 1;
            for (int i = 0; i < j; i++)
            {
                exists = false;
                foreach (List<string> s in students)
                {
                    if (int.Parse(s[0]) == matric)
                    {
                        exists = true;
                        break;
                    }
                }
                if (exists)
                {
                    matric++;
                    j++;
                }
            }
            bool[] expected = { false, false, false, false };

            // Act
            bool[] actual = m.enroleUpdate(code, matric);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Enrolement failed as student non-existant");
            }
        }
    
    }
}
