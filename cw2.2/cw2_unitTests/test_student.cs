﻿using cw2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace cw2_unitTests
{
    [TestClass]
    public class test_student
    {
        /// <summary>
        /// Tests the creation or update of student with valid variables
        /// </summary>
        [TestMethod]
        public void test_createUpdate_pass()
        {
            // Arrange
            int matric = 1234;
            string fname = "Jonny";
            string lname = "West";
            string address = "EH1 1AA, Edinburgh City";
            string email = "40129963@live.napier.ac.uk";
            bool[] expected = { true, true, true, true, true };

            // Act
            student s = new student();
            bool[] actual = s.createUpdate(matric, fname, lname, address, email);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Student created correctly.");
            }
        }

        /// <summary>
        /// Tests the creation or update of student with invalid variables
        /// </summary>
        [TestMethod]
        public void test_createUpdate_fail()
        {
            // Arrange
            int matric = 9001;
            string fname = "overTwentyCharactersLong";
            string lname = null;
            string address = "";
            string email = "40129963_live.napier.ac.uk";
            bool[] expected = { false, false, false, false, false };

            // Act
            student s = new student();
            bool[] actual = s.createUpdate(matric, fname, lname, address, email);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Student not created: Values invalid.");
            }
        }
    }
}
