﻿using cw2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;

namespace cw2_unitTests
{
    [TestClass]
    public class test_university
    {
        university n = university.getInstance();

        /// <summary>
        /// Tests the creation of a new student with correct details
        /// </summary>
        [TestMethod]
        public void test_createUpdateStudent_Staff_createStudent_pass()
        {
            // Arrange
            int student_staff = 0;
            int matric = 1000;
            List<List<string>> students = n.existingStudent_Staff(student_staff);
            int j = 1;
            bool exists;
            // Ensures that the matric value used is not already recorded in the database
            for (int i = 0; i < j; i++)
            {
                exists = false;
                foreach (List<string> s in students)
                {
                    if (int.Parse(s[0]) == matric)
                    {
                        exists = true;
                        break;
                    }
                }
                if (exists)
                {
                    matric++;
                    j++;
                }
            }
            string fname = "Jonny";
            string lname = "West";
            string address = "EH1 1AA, Edinburgh City";
            string email = "40129963@live.napier.ac.uk";
            bool[] expected = { true, true, true, true, true };

            // Act
            bool[] actual = n.createUpdateStudent_Staff(student_staff, matric, fname, lname, address, email);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Student created correctly.");
            }
        }

        /// <summary>
        /// Tests the update of an existing student with correct details
        /// </summary>
        [TestMethod]
        public void test_createUpdateStudent_Staff_updateStudent_pass()
        {
            // Arrange
            int student_staff = 0;
            List<string> existingStudent = n.existingStudent_Staff(student_staff)[0];
            int matric = int.Parse(existingStudent[0]);
            string fname = "Jonny";
            string lname = "West";
            string address = "EH1 1AA, Edinburgh City";
            string email = "40129963@live.napier.ac.uk";
            bool[] expected = { true, true, true, true, true };

            // Act
            bool[] actual = n.createUpdateStudent_Staff(student_staff, matric, fname, lname, address, email);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Student updated correctly.");
            }
        }

        /// <summary>
        /// Tests the creation of a new staff with correct details
        /// </summary>
        [TestMethod]
        public void test_createUpdateStudent_Staff_createStaff_pass()
        {
            // Arrange
            int student_staff = 1;
            int payroll = 9000;
            // Ensures that the payroll value used is not already recorded in the database
            List<List<string>> staffs = n.existingStudent_Staff(student_staff);
            int j = 1;
            bool exists;
            for (int i = 0; i < j; i++)
            {
                exists = false;
                foreach (List<string> s in staffs)
                {
                    if (int.Parse(s[0]) == payroll)
                    {
                        exists = true;
                        break;
                    }
                }
                if (exists)
                {
                    payroll++;
                    j++;
                }
            }
            string fname = "Jonny";
            string lname = "West";
            string address = "EH1 1AA, Edinburgh City";
            string email = "40129963@live.napier.ac.uk";
            string dept = "Computing";
            string role = "Professor";
            bool[] expected = { true, true, true, true, true, true, true };

            // Act
            bool[] actual = n.createUpdateStudent_Staff(student_staff, payroll, fname, lname, address, email, dept, role);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Staff created correctly.");
            }
        }

        /// <summary>
        /// Tests the update of an existing staff with correct details
        /// </summary>
        [TestMethod]
        public void test_createUpdateStudent_Staff_updateStaff_pass()
        {
            // Arrange
            int student_staff = 1;
            List<string> existingStaff = n.existingStudent_Staff(student_staff)[0];
            int payroll = int.Parse(existingStaff[0]);
            string fname = "Jonny";
            string lname = "West";
            string address = "EH1 1AA, Edinburgh City";
            string email = "40129963@live.napier.ac.uk";
            string dept = "Computing";
            string role = "Professor";
            bool[] expected = { true, true, true, true, true, true, true };

            // Act
            bool[] actual = n.createUpdateStudent_Staff(student_staff, payroll, fname, lname, address, email, dept, role);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Staff updated correctly.");
            }
        }

        /// <summary>
        /// Tests the creation of a new module with correct details
        /// </summary>
        [TestMethod]
        public void test_createUpdateModule_createModule_pass()
        {
            // Arrange
            int codeInt = 1;
            List<List<string>> modules = n.existingModules();
            int j = 1;
            bool exists;
            // Ensures that the code value used is not already recorded in the database
            for (int i = 0; i < j; i++)
            {
                exists = false;
                foreach (List<string> mo in modules)
                {
                    if (mo[0] == codeInt.ToString())
                    {
                        exists = true;
                        break;
                    }
                }
                if (exists)
                {
                    codeInt++;
                    j++;
                }
            }
            string code = codeInt.ToString();
            string name = "Computing";
            int leader = 0;
            bool[] expected = { true, true, true };

            // Act
            bool[] actual = n.createUpdateModule(code, name, leader);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Module created correctly.");
            }
        }

        /// <summary>
        /// Tests the update of an existing module with correct details
        /// </summary>
        [TestMethod]
        public void test_createUpdateModule_updateModule_pass()
        {
            // Arrange
            List<string> existingModule = n.existingModules()[0];
            string code = existingModule[0];
            string name = "Computing";
            int leader = 0;
            bool[] expected = { true, true, true };

            // Act
            bool[] actual = n.createUpdateModule(code, name, leader);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Module updated correctly.");
            }
        }

        /// <summary>
        /// Tests the creation of a new enrolement with no assigned mark
        /// </summary>
        [TestMethod]
        public void test_enroleUpdate_noMark_pass()
        {
            // Arrange
            List<string> existingModule = n.existingModules()[0];
            string code = existingModule[0];
            int student = 0;
            bool[] expected = { true, false, false, true };

            // Act
            bool[] actual = n.enroleUpdate(code, student);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Student enroled correctly.");
            }
        }

        /// <summary>
        /// Tests the creation of a new enrolement with a valid assigned mark
        /// </summary>
        [TestMethod]
        public void test_enroleUpdate_withMark_pass()
        {
            // Arrange
            List<string> existingModule = n.existingModules()[0];
            string code = existingModule[0];
            int student = 0;
            int mark = 90;
            bool[] expected = { true, false, true, true };

            // Act
            bool[] actual = n.enroleUpdate(code, student, mark);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Student enroled correctly.");
            }
        }
    }
}
