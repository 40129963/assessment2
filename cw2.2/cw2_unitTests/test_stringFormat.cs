﻿using cw2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace cw2_unitTests
{
    [TestClass]
    public class test_stringFormat
    {
        [TestMethod]
        public void test_formatForSQL_pass_a()
        {
            // Arrange
            string input = "one'two'three'";
            string expected = "one''two''three''";
            // Assert
            Assert.AreEqual(stringFormat.formatForSQL(input), expected, "String was formatted correctly for SQL input.");
        }

        [TestMethod]
        public void test_formatForSQL_pass_b()
        {
            // Arrange
            string input = null;
            string expected = null;
            // Assert
            Assert.AreEqual(stringFormat.formatForSQL(input), expected, "String was not changed as null.");
        }

        [TestMethod]
        public void test_formatForSQL_pass_c()
        {
            // Arrange
            string input = "nothingToChange";
            string expected = input;
            // Assert
            Assert.AreEqual(stringFormat.formatForSQL(input), expected, "String was not changed.");
        }
    }
}
