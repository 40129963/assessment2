﻿using cw2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace cw2_unitTests
{
    [TestClass]
    public class test_staff
    {
        /// <summary>
        /// Tests the creation or update of staff with valid variables
        /// </summary>
        [TestMethod]
        public void test_createUpdate_pass()
        {
            // Arrange
            int payroll = 9876;
            string fname = "Jonny";
            string lname = "West";
            string address = "EH1 1AA, Edinburgh City";
            string email = "40129963@live.napier.ac.uk";
            string dept = "Computing";
            string role = "Professor";
            bool[] expected = { true, true, true, true, true, true, true };

            // Act
            staff s = new staff();
            bool[] actual = s.createUpdate(payroll, fname, lname, address, email, dept, role);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Student created correctly.");
            }
        }

        /// <summary>
        /// Tests the creation or update of staff with invalid variables
        /// </summary>
        [TestMethod]
        public void test_createUpdate_fail()
        {
            // Arrange
            int payroll = -1;
            string fname = "overTwentyCharactersLong";
            string lname = null;
            string address = "";
            string email = "40129963_live.napier.ac.uk";
            string dept = "";
            string role = "notFromComboBox";
            bool[] expected = { false, false, false, false, false, false, false };

            // Act
            staff s = new staff();
            bool[] actual = s.createUpdate(payroll, fname, lname, address, email, dept, role);

            // Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], "Staff not created: values invalid.");
            }
        }
    }
}
